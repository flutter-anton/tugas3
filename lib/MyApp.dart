import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            leading: Padding(
              padding: EdgeInsets.only(left: 12),
              child: IconButton(
                icon: Icon(Icons.menu),
                onPressed: (){
                  print('Menu');
                },
              ),
            ),
            title: Row(
              //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Antonie App 2'),
                ]
            ),

            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.favorite),
                onPressed: (){
                  print('Suka');
                },
              ),

              IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: (){
                  print('Prefences');
                },
              ),
            ],

            backgroundColor: Colors.lightBlue,
          ),
          body: PickPic(),
          floatingActionButton: FloatingActionButton(
            onPressed: (){
            },
            child: Icon(Icons.done),
            backgroundColor: Colors.pink,
          ),
        ),
      );
  }
}


class PickPic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment(0,-0.9),
            child: Image.asset('assets/meme_self2.png',
              height: 350,
              width: 475,
            ),
          ),
          Container(
            alignment: Alignment(0,0.6),
            child: Text(
              'I Gusti Nyoman Anton Surya Diputra\n'
                  '1915051027',
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
